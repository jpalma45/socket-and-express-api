const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);

app.set('port', process.env.PORT || 3000);

app.get('/port_node', (req, res, next) => {
    res.status(200).json(app.get('port'));
});

app.get('/port_socket', (req, res, next) => {
    res.status(200).json({
        message: 'Johan palma',
        data: {}
    });
});

io.sockets.on('connection', (socket) => {
    socket.on('test', (dataUser) => {
        socket.emit('testResponse',dataUser);
    });

    socket.on('disconnect', () => {
        console.log(`Socket disconnect: ${socket.id}`);
    });
})

server.listen(app.get('port'));